package circularList;

import java.util.Iterator;

import nodo.Nodo;

public class CircularList<T> implements Iterable<T> {
  private Nodo<T> sent;
  private Nodo<T> act;
  

public CircularList() {
	sent = new Nodo<T>();
	act = new Nodo<T>();
	sent.setIndex(-1);
	act.setIndex(-1);
}
public CircularList(T value){
	sent.setNext(new Nodo<T>(value));
	act = sent.getNext();
	act.setNext(act);
}

	public Nodo<T> getLast(){
		Nodo<T> tmp = sent.getNext();
		if(tmp !=null)
		while(tmp.getNext()!= sent.getNext()){
          tmp = tmp.getNext();			
		}
		return tmp;
	}
	
	
	public void print(){
		Nodo<T> temp = sent.getNext();
		if(!isEmpty()){
			
			System.out.print(temp.getValue()+" ");
		while(temp.getNext()!= sent.getNext()){
			System.out.print(temp.getNext().getValue()+" ");
          temp = temp.getNext();			
		}
		}
		
	}
	public boolean remove(T value){
		act = search(value);
		if(act != null){
			Nodo<T> temp = searchBefore(value);
			
			if(act.equals(sent.getNext())){
				if(act.getNext()== act)
					sent.setNext(null);
				else{
					sent.setNext(act.getNext());
					temp.setNext(act.getNext());
				}
				return true;
			}
			
			else
				temp.setNext(act.getNext());
			reIndex();	
		}
		return false;
	}
	
	
	public void addFirst(T value){
		Nodo<T> niew = new Nodo(value);
		if(!isEmpty()){
			niew.setNext(sent.getNext());
			getLast().setNext(niew);
			//qutar esto para el addLast
			sent.setNext(niew);
		}
		else{
			sent.setNext(niew);
			niew.setNext(niew);
		}
		reIndex();
	}
	public void addLast(T value){
		Nodo<T> niew = new Nodo(value);
		if(!isEmpty()){
			niew.setNext(sent.getNext());
			getLast().setNext(niew);
			
		}
		else{
			sent.setNext(niew);
			niew.setNext(niew);
		}
		reIndex();
	}
	public boolean addAfter(T value,T nv){
		Nodo<T> temp = search(value);
		if(temp!= null){
			Nodo<T>nu = new Nodo<T>(nv);
			nu.setNext(temp.getNext());
			temp.setNext(nu);
			reIndex();
			return true;
		}
		return false;
	}
	public boolean addBefore(T value,T nv){
		Nodo<T> temp = searchBefore(value);
		if(temp!= null){
			Nodo<T>nu = new Nodo<T>(nv);
			if(temp.getNext()== sent.getNext()){
				sent.setNext(nu);
			}
			nu.setNext(temp.getNext());
			temp.setNext(nu);
			reIndex();
			return true;
		}
		return false;
	}
	
	
	public Nodo<T> search(T value){
		if(!isEmpty())
		return search(value , sent.getNext());
		else return null;
	}
	
	public Nodo<T> search(T value , Nodo act){
		//System.out.println("comparando "+value+" con "+act.getNext().getValue());
		if(act.getNext().getValue().equals(value)){
			return act.getNext();	
		}
		if(act.getNext()== sent.getNext()){
			return null;
		}
		return search(value,act.getNext());
	}
	
	
	
	public Nodo<T> searchBefore(T value){
		Nodo temp = search(value);
		if(temp!= null){
			return searchBefore((T)value ,(Nodo<T>) temp);
		}
		else return null;
	}
	
	public Nodo<T> searchBefore(T value , Nodo<T> act){
		//System.out.println("comparando "+value+" con "+act.getValue());
		if(act.getNext().getValue().equals(value)){
			return act;	
		}
	
		return searchBefore(value,act.getNext());
	}
	
	public Nodo<T> searchBefore(Nodo value){
		Nodo<T> temp = search((T)value.getValue());
		if(temp!= null){
		    return searchBefore(value , temp);
		}
		else return null;
	}
	
	public Nodo<T> searchBefore(Nodo value , Nodo<T> act){
		if(act.getNext().equals(value)){
			return act;	
		}
		return searchBefore((Nodo<T>)value,(Nodo<T>)act.getNext());
	}
	
	
	 public boolean replace (T val , T nu){
		 Nodo<T> temp  = search(val);
		 if (temp != null){
			 temp.setValue(nu);
		 return true;
		 }
	
		 return false ;
	 }
	
	public void clear (){
		while (!isEmpty()){
			removeFirst();
		}
	}
	
	
	public void removeFirst() {
		if(sent.getNext() == sent.getNext().getNext())
			sent.setNext(null);
		else{
			getLast().setNext(sent.getNext().getNext());
			sent.setNext(sent.getNext().getNext());
		}
		reIndex();
		
	}
	public void removeLast(){
		if(sent.getNext() == sent.getNext().getNext())
			sent.setNext(null);
		else{
			searchBefore(getLast()).setNext(sent.getNext());
		}
	}
	public Nodo<T> getFirst(){
		return sent.getNext();
	}
	
	//test this mothef fucker
	
	public void removeAfter(T value){
		Nodo<T> tmp = search(value);
		if(tmp!=null){
			remove(tmp.getNext().getValue());
		reIndex();
		}
	}
	public void removeBefore(T value){
		Nodo<T> tmp = searchBefore(value);
		if(tmp!=null){
			remove(tmp.getValue());
			reIndex();
		}
	}
	
	
	
	
	public void reIndex(){
		Nodo<T> temp = sent.getNext();
		int cont = 0;
		if(!isEmpty())
		while(temp.getNext() != sent.getNext()){
			temp.setIndex(cont++);		
			temp = temp.getNext();
		}
	}
	public int size(){
		Nodo<T> temp = sent.getNext();
		if(isEmpty())return 0;
		int cont = 1;
		while(temp.getNext() != sent.getNext()){
			cont++;
			temp = temp.getNext();
		}
		return cont;
	}
	public int indexOf(T value){
		Nodo<T> tmp = search(value);
		return (int) ((tmp!=null)?tmp.getIndex():-1);
	}

	public boolean isEmpty(){
		return (sent.getNext()  == null)? true:false;
	}
	
	
	
	public Iterator<T> iterator() {
		
		return new Iterator<T>(){
			int i = -1;
		
			@Override
			public boolean hasNext() {
				
					return getByIndex(i).getNext()!=sent.getNext() ? true : false ;
			
			}

			@Override
			public T next() {
				
					return (T) getByIndex(++i).getValue();
				
			}

			private Nodo<T> getByIndex(int j) {
				Nodo<T> temp =sent.getNext();
				for (int x = 0; x < j; x++) {
					temp = temp.getNext();
				}
				return temp;
			}
		
			
		};
 }
	
	
	
	
	
}
